const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const merge = require('webpack-merge')
const sass = require('./webpack/sass');
const pug = require('./webpack/pug');
const extractCSS = require('./webpack/css.extract')

const PATHS = { 
    source: path.join(__dirname, 'source'),
    build: path.join(__dirname, 'build')
};

module.exports = merge([
    {
    entry: PATHS.source + '/index.js',
    output: { 
        path: PATHS.build,
        filename: 'js/[name].js'
},
    plugins: [
        new HtmlWebpackPlugin({
            template: PATHS.source + '/index.pug'
        })
    ],
    },
    pug(),
    extractCSS()
]);