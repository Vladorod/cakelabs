const Extract = require('extract-text-webpack-plugin');

module.exports = function (paths) { 
    return { 
        module: { 
            rules: [
                { 
                   test: /\.sass$/,
                   include: paths,
                   use: Extract.extract({
                    publicPath: '../',
                    fallback: 'style-loader',
                    use: ['css-loader','sass-loader'],
                   }),
                },
                { 
                   test: /\.css$/,
                   include: paths,
                   use: Extract.extract({
                    fallback: 'style-loader',
                    use: 'css-loader',
                    }),
                },
            ],
        },
        plugins: [
            new Extract('./css/mine.css')
        ],
    };
}